var mongoose = require('mongoose');
var ok = require('okay');

var PrintersSchema = new mongoose.Schema({
  Name: {
    type: String,
    unique: true
  },
  UniqueString: {
    type: String

  },
  Jobs: [{
    JobName: String,
    JobPath: String,
    PrintTimeInHours: String,
    JobStatus: {
      type: String,
      enum: ['New', 'Started', 'Finish', 'Failed'],
      default: 'New'
    }
  }]
});

var PrinterModel = module.exports = mongoose.model('printers', PrintersSchema);

module.exports.addPrinter = (Printers, cb) => {
  Printers.save(ok(cb , function(PrinterData) {
      cb(null, PrinterData);
  }));
}

module.exports.getPrinter = (cb) => {
  PrinterModel.find(ok(cb , function(PrinterData){
      cb(null, PrinterData);
  }));
}

module.exports.GetNextJobDetails = (PrinterUniqueString, cb) => {

  const query = { "UniqueString": PrinterUniqueString };
  const projection = { Jobs: { $elemMatch: { "JobStatus": "New" } } };
  PrinterModel.findOne(query, projection, ok(cb , function(PrinterData) {
      cb(null, PrinterData);
  }));
}

module.exports.GetJobInfo = (PrinterUniqueString, JobId, cb) => {

  const query = { "UniqueString": PrinterUniqueString };
  const projection = { Jobs: { $elemMatch: { "_id": JobId } } };
  PrinterModel.findOne(query, projection, ok(cb , function(PrinterData){
      cb(null, PrinterData);
  }));
}



module.exports.getOnePrinter = (PrinterUniqueString, cb) => {
  PrinterModel.findOne({ 'UniqueString': PrinterUniqueString }, ok(cb , function( PrinterData) {
      cb(null, PrinterData);
  }));
}


module.exports.removePrinter = (id, cb) => {
  PrinterModel.deleteOne({ '_id': id }, ok(cb , function( PrinterData) {
      cb(null, PrinterData);
  }));
}

module.exports.addJobToPrinter = (PrinterUniqueString, JobName, JobPath, cb) => {
  var filter = null;
  if (PrinterUniqueString) {
    filter = { "UniqueString": PrinterUniqueString };
  }

  PrinterModel.findOneAndUpdate(
    filter,
    { "$push": { "Jobs": { "JobName": JobName, "JobPath": JobPath } } }, { "new": true }, ok(cb , function(Printer){
      if(Printer)
        cb(null, Printer.Jobs[Printer.Jobs.length - 1]);
      else
       cb(null, {"error" : "Printer " + PrinterUniqueString+ " doesn't exist"})
    }))

}

module.exports.RemoveFirstJob = (PrinterUniqueString, cb) => {
  PrinterModel.updateOne(
    { "UniqueString": PrinterUniqueString },
    { "$pop": { "Jobs": -1 } }, ok(cb , function(){
        cb(null);
    }))

}


module.exports.ChangeJobStatus = (PrinterUniqueString, JobId, JobStatus, cb) => {
  const query = { "UniqueString": PrinterUniqueString, "Jobs._id": JobId };
  const updateDocument = { $set: { "Jobs.$.JobStatus": JobStatus } };
  const options = { upsert: true };
  PrinterModel.updateOne(query, updateDocument, options, ok(cb , function() {
      cb(null);
  }))

}


module.exports.UpdateJobMetaData = (JobId, TimeStr, cb) => {
  const query = { "Jobs._id": JobId };
  const updateDocument = { $set: { "Jobs.$.PrintTimeInHours": TimeStr } };
  const options = { upsert: true };
  PrinterModel.updateOne(query, updateDocument, options, ok(cb , function(){
      cb(null);
  }))

}




