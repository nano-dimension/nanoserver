var express = require('express');
var PrinterModel = require('../models/PrintersModel.js');
const multer = require('multer');
const os = require('os');


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, os.tmpdir())
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

var upload = multer({ storage: storage })

var router = express.Router();
var ok = require('okay');

router.get('/home', (req, res , next) => {
  res.render('demo');
});


router.get('/GetNextJobDetails', (req, res , next) => {
  var PrinterIdentifier = req.query.PrinterIdentifier;
  PrinterModel.GetNextJobDetails(PrinterIdentifier, ok(next,function(PrinterData) {
      if (PrinterData && PrinterData.Jobs.length > 0) {
        res.jsonp(PrinterData.Jobs[0]);
      } else {
        res.sendStatus(204); //204 NO CONTENT
      }
  }));

});

router.get('/GetJobInfo', (req, res , next) => {
  var PrinterIdentifier = req.query.PrinterIdentifier;
  var JobId = (req.query.JobId);
  PrinterModel.GetJobInfo(PrinterIdentifier, JobId, ok(next,function(PrinterData) {
      if (PrinterData && PrinterData.Jobs.length > 0) {
        res.jsonp(PrinterData.Jobs[0]);
      } else {
        res.sendStatus(204); //204 NO CONTENT
      }
  }));
});



router.get('/GetJob', (req, res , next) => {
  var PrinterIdentifier = (req.query.PrinterIdentifier);
  var JobId = (req.query.JobId);
  PrinterModel.GetJobInfo(PrinterIdentifier, JobId, ok(next,function(PrinterData) {
      if (PrinterData && PrinterData.Jobs.length > 0) {
        res.download(PrinterData.Jobs[0].JobPath);
      } else {
        res.sendStatus(204); //204 NO CONTENT
      }
  }));
});

router.get('/StartJob', (req, res , next) => {
  var PrinterIdentifier = (req.query.PrinterIdentifier);
  PrinterModel.StartJob(PrinterIdentifier, ok(next,function(PrinterData) {
      res.sendStatus(200);
  }));

});

router.get(['/NotifyJobActive', '/CancelJob'], (req, res , next) => {
  var PrinterIdentifier = (req.query.PrinterIdentifier);
  var JobId = (req.query.JobId);
  PrinterModel.ChangeJobStatus(PrinterIdentifier, JobId, "Started", ok(next,function(PrinterData) {
      res.sendStatus(200);
  }));
});


router.post('/NewJob', upload.single('myFile'), (req, res, next) => {
  const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
  PrinterModel.addJobToPrinter(req.body.UniqueString, req.body.JobName, file.path, ok(next , function(doc ) {
    res.json(doc);
  }));

})


router.post('/AddPrinter', (req, res , next) => {
  var PM = new PrinterModel({
    Name: req.body.PrinterName,
    UniqueString: req.body.UniqueString
  });
  PrinterModel.addPrinter(PM, ok(next , function( PrinterData) {
      res.json({ msg: 'success' });
  }));
});

//Yair Todo: change it to PUT request
router.get('/UpdateJobMetadata', (req, res , next) => {
   PrinterModel.UpdateJobMetaData(req.query.ServerJobId, req.query["PCB:PrintTimeInHours"], ok(next,function(PrinterData) {
      res.sendStatus(200);
  }));

});



router.get('/GetPrinters', (req, res, next) =>{
  PrinterModel.getPrinter(ok(next, function(PrinterData){
    res.json({ msg: 'success', data: PrinterData });
  }))
})



router.delete('/RemovePrinter', (req, res , next) => {
  PrinterModel.removePrinter(req.body.id, ok(next,function(PrinterData){
      res.json({ msg: 'success' });
  }));
});

module.exports = router;
